const keyMirror = require('react/lib/keyMirror');

module.exports = keyMirror({
     SELECT_ROUND: null,
     SWAP_ROUNDS: null,
     SWAP_HOMES: null
  });
