var _ = require('underscore');

module.exports = Object.freeze({
    getColor: function(name) {
        switch(name) {
            case 'A': return 'red';
            case 'B': return 'green';
            case 'C': return 'magenta';
            case 'D': return 'blue';
            default: return 'black';
        }
    },

    getPath: function(name, path, coord) {
        var coord = _.map(path, function(el) {
            if(el[1]) {
                return coord[el[0]]
            } else {
                return coord[name]
            }
        });
        return _.first(_.zip(coord, _.rest(coord)), path.length - 1);
    },

    getLength: function(name, pathRaw, coord) {
        var path = this.getPath(name, pathRaw, coord);
        var dist = _.chain(path)
            .map(function(p) {
                return Math.sqrt((p[0].x-p[1].x)*(p[0].x-p[1].x)+(p[0].y-p[1].y)*(p[0].y-p[1].y));
            })
            .reduce(function(a,b) {return a + b}, 0);
        return Math.round(dist);
    },

    getTotalLength: function(path, coord) {
        return _.chain(path)
            .map(function(p,n) {
                return this.getLength(n, p, coord)}.bind(this))
            .reduce(function(a,b) {return a+ b});
    }
});