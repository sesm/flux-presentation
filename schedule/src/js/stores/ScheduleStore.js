var assign = require('object-assign');
var EventEmitter = require('events').EventEmitter;
var Constants = require('../constants/AppConstants');
var Dispatcher = require('../dispatchers/AppDispatcher');
var AppDispatcher = require('../dispatchers/AppDispatcher');
var _ = require('underscore');

var CHANGE_EVENT = 'change';

var _names = {A: 'Armadillos', B: 'Behemoths', C: 'Capibaras', D: 'Dolphins'};
var _coordinates = {
    A: {x: 50, y: 20},
    B: {x: 130, y: 50},
    C: {x: 100, y: 150},
    D: {x: 180, y: 180}
};
var _schedule = {
    A: [['B', true], ['C', true], ['D', false], ['B', false], ['D', true], ['C', false]],
    B: [['A', false], ['D', true], ['C', false], ['A', true], ['C', true], ['D', false]],
    C: [['D', false], ['A', false], ['B', true], ['D', true], ['B', false], ['A', true]],
    D: [['C', true], ['B', false], ['A', true], ['C', false], ['A', false], ['B', true]]
};
var _activeRound = null;

var ScheduleStore = assign({}, EventEmitter.prototype, {

    getNames: function() {
        return _names;
    },
    getCoord: function() {
        return _coordinates;
    },
    getSchedule: function() {
        return _schedule;
    },
    getActiveRound: function() {
        return _activeRound;
    },

    addChangeListener: function(callback) {
        this.on(CHANGE_EVENT, callback);
    },

    removeChangeListener: function(callback) {
        this.removeListener(CHANGE_EVENT, callback);
    },

    emitChange: function() {
        this.emit(CHANGE_EVENT);
    }
});

AppDispatcher.register(function(action) {
    switch(action.actionType) {
        case Constants.SWAP_ROUNDS:
            _activeRound = null;
            console.log('Swap round ' + action.r1 + ' ' + action.r2);
            _.forEach(_schedule, function(path, index) {
                var r1 = path[action.r1-1];
                path[action.r1-1] = path[action.r2-1];
                path[action.r2-1] = r1;
            });
            ScheduleStore.emitChange();
            break;

        case Constants.SELECT_ROUND:
            _activeRound = action.round;
            ScheduleStore.emitChange();
            break;

        case Constants.SWAP_HOMES:
            _activeRound = null;
            _.forEach(_schedule, function(path, index) {
                if(index == action.t1 ||
                   index == action.t2) {
                    _.forEach(path, function(g) {
                        if(g[0] == action.t1 ||
                           g[0] == action.t2) {
                            g[1] = !g[1];
                        }
                    })
                }
            });
            ScheduleStore.emitChange();

        default:
            // no op
    }
});

module.exports = ScheduleStore;
