var AppDispatcher = require('../dispatchers/AppDispatcher');
var Constants = require('../constants/AppConstants');

module.exports = {
    selectRound: function(n) {
        AppDispatcher.dispatch({
            actionType: Constants.SELECT_ROUND,
            round: n
        });
    },

    swapRounds: function(n,m) {
        AppDispatcher.dispatch({
            actionType: Constants.SWAP_ROUNDS,
            r1: n,
            r2: m
        });
    },

    swapHomes: function(t1, t2) {
        AppDispatcher.dispatch({
            actionType: Constants.SWAP_HOMES,
            t1: t1,
            t2: t2
        });
    },
};
