var React = require('react');
var ScheduleStore = require('../stores/ScheduleStore');
var Table = require('./Table.jsx');
var Map = require('./Map.jsx');
var Path = require('../util/Path');
var _ = require('underscore');

var App = React.createClass({

  getInitialState: function() {
    return {
        names: ScheduleStore.getNames(),
        coord: ScheduleStore.getCoord(),
        schedule: ScheduleStore.getSchedule(),
        activeRound: ScheduleStore.getActiveRound()
    }
  },

  componentDidMount: function() {
    ScheduleStore.addChangeListener(this._onChange);
  },

  componentWillUnmount: function () {
    ScheduleStore.removeChangeListener(this._onChange);
  },

  _onChange: function() {
      this.setState({
        names: ScheduleStore.getNames(),
        coord: ScheduleStore.getCoord(),
        schedule: ScheduleStore.getSchedule(),
        activeRound: ScheduleStore.getActiveRound()
    });
  },

  render: function() {
    return <div>
        {_.union(
            _.map(this.state.schedule, function(path, name) {
                return <Map coord={this.state.coord}
                    path={Path.getPath(name, path, this.state.coord)}
                    color={Path.getColor(name)}/>
            }.bind(this)),
            [<Table schedule={this.state.schedule}
                coord={this.state.coord} names={this.state.names}
                active={this.state.activeRound}/>])}

    </div>;
  }

});

module.exports = App;
