var React = require('react');
var Path = require('../util/Path');
var ActionCreators = require('../actions/ActionCreators');
var _ = require('underscore');

var Table = React.createClass({
    createHeader: function()  {
        var rounds = _.map(
            _.range(this.props.schedule.A.length),
            function(n) {
                return <td key={n+1}
                    onClick={function() {
                        if(this.props.active == null) {
                            ActionCreators.selectRound(n+1);
                        } else {
                            ActionCreators.swapRounds(n+1, this.props.active);
                        }
                    }.bind(this)}>
                    {n+1}</td>;
            }.bind(this)
        );
        return <tr key="header">
            {_.union(
                [<td key="round">Round</td>],
                rounds,
                [<td key="distance">Travel distance:</td>])}
        </tr>
    },

    createTeamRow: function(path, name) {
        return <tr>{_.union(
            [<td>{this.props.names[name]}</td>],
            _.map(path, function(g, ix) {
                css = ix+1 == this.props.active ? "selected" : "";
                if(g[1]) {
                    return <td className={css}
                        onClick={function() {
                            ActionCreators.swapHomes(name, g[0])
                        }}
                    ><b>{g[0]}</b></td>
                } else {
                    return <td className={css}
                        onClick={function() {
                            ActionCreators.swapHomes(name, g[0])}}
                    >{g[0]}</td>
                }}.bind(this)),
            [<td>{Path.getLength(name, path, this.props.coord)}</td>])}
        </tr>
    },

    render: function() {
        return <table>{_.union(
            [this.createHeader()],
            _.map(this.props.schedule, this.createTeamRow),
            _.union(
                _.times(7, function() {return <td></td>}),
                [<td><b>{'Total: ' + Path.getTotalLength(this.props.schedule, this.props.coord)}</b></td>])
        )}</table>
    }
});

module.exports = Table;