var React = require('react');
var _ = require('underscore');

var Map = React.createClass({

    getCities: function(coord) {
        return _.flatten(_.map(
            coord,
            function(coord, name) {
                labelxOff = coord.x >= 100 ? 5 : -5;
                labelyOff = coord.y >= 100 ? 5 : -5;
                return [
                    <circle cx={coord.x} cy={coord.y} r={2} strokeWidth={0} fill="black"></circle>,
                    <text x={coord.x + labelxOff} y={coord.y + labelyOff} fill="black">{name}</text>
                ]
            }));
    },

    getLines: function(path, color) {
        return _.map(path, function(p) {
            return <line x1={p[0].x} y1={p[0].y} x2={p[1].x} y2={p[1].y} strokeWidth="2" stroke={color}/>
        });
    },

    render: function() {
        return <svg height="200" width="200">
            {_.union(
                this.getLines(this.props.path, this.props.color),
                this.getCities(this.props.coord))}
        </svg>
    }
});

module.exports = Map;
